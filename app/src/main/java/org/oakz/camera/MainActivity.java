package org.oakz.camera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    public Button fotografar;
    public ImageView foto;
    public Uri caminhoArquivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fotografar = (Button) findViewById(R.id.fotografar);
        foto = (ImageView) findViewById(R.id.foto);

        fotografar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String caminhoFoto = Environment.DIRECTORY_PICTURES + "/SENAC_" + System.currentTimeMillis() + ".png";
                File arquivo = Environment.getExternalStoragePublicDirectory(caminhoFoto);
                MainActivity.this.caminhoArquivo = Uri.fromFile(arquivo);
                irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, MainActivity.this.caminhoArquivo);
                startActivityForResult(irParaCamera, 13);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 13:
                if (resultCode == Activity.RESULT_OK){
                    Bitmap imagem_bitmap = BitmapFactory.decodeFile(caminhoArquivo.getPath());
                    Bitmap imagem_tratada = Bitmap.createScaledBitmap(imagem_bitmap, 100, 100, true);
                    foto.setImageBitmap(imagem_tratada);
                }
            break;

        }
    }
}
